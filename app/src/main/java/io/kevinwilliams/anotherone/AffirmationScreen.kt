package io.kevinwilliams.anotherone

import android.content.res.Configuration
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.VerticalPager
import io.kevinwilliams.anotherone.ui.components.RoundButton
import io.kevinwilliams.anotherone.ui.icons.outlined.FilterList
import io.kevinwilliams.anotherone.ui.icons.outlined.Style
import io.kevinwilliams.anotherone.ui.theme.AnotherOneTheme

@OptIn(ExperimentalPagerApi::class)
@Composable
fun AffirmationScreen(
    affirmations: List<AffirmationItem>
) {
    Box {
        /** ====== Background Image ====== */
        Image(
            painterResource(id = R.drawable.background_frosty_field),
            contentDescription = null,
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.matchParentSize()
        )
        Box(modifier = Modifier.padding(8.dp)) {
            /** ====== Affirmation Pager ====== */
            VerticalPager(count = affirmations.count()) { page ->
                AffirmationCard(affirmation = affirmations[page])
            }
            /** ====== UI ====== */
            Row {
                Spacer(modifier = Modifier.weight(1f))
                RoundButton(
                    icon = Icons.Outlined.Style,
                    contentDescription = "Change style",
                    onClick = { goToSettings() }
                )
                Spacer(modifier = Modifier.width(8.dp))
                RoundButton(
                    icon = Icons.Outlined.FilterList,
                    contentDescription = "Open settings",
                    onClick = { goToSettings() }
                )
            }
        }
    }
}

private fun goToSettings() {
}

@Preview(showBackground = true)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode"
)
@Composable
fun AffirmationPreview() {
    AnotherOneTheme {
        AffirmationScreen(affirmations = affirmationList)
    }
}