package io.kevinwilliams.anotherone

data class AffirmationItem(val text: String, val tags: List<String>? = null) {}

val affirmationList = listOf<AffirmationItem>(
    AffirmationItem(
        text = "I am surrounded by love and everything is fine."
    ),
    AffirmationItem(
        text = "My heart is always open, and I radiate love."
    ),
    AffirmationItem(
        text = "All my relationships are long-lasting and loving."
    ),
    AffirmationItem(
        text = "I see everything with loving eyes, and I love everything I see."
    ),
    AffirmationItem(
        text = "I deserve love and I get it in abundance."
    ),
    AffirmationItem(
        text = "Everywhere I go, I find love. Life is joyous."
    ),
    AffirmationItem(
        text = "I am surrounded by love every day of my life."
    ),
    AffirmationItem(
        text = "I love myself and because of that everyone loves me."
    ),
    AffirmationItem(
        text = "I attract love in everything I do."
    ),
    AffirmationItem(
        text = "I freely express the love I feel to those I love, and they return my gesture."
    ),
    AffirmationItem(
        text = "I am blessed with a beautiful family."
    ),
    AffirmationItem(
        text = "I get the help I need, when I need it, from various sources. My support system is strong and loving.",
        tags = listOf("help", "support", "family")
    ),
    AffirmationItem(
        text = "I cherish my friendships."
    ),
    AffirmationItem(
        text = "My friends empower me."
    ),
    AffirmationItem(
        text = "I am available for my friends and family."
    ),
    AffirmationItem(
        text = "My relationships are loving and harmonious."
    ),
    AffirmationItem(
        text = "I always stay in contact with my friends."
    ),
    AffirmationItem(
        text = "I am a compassionate, forgiving friend."
    ),
    AffirmationItem(
        text = "I am the friend I would want for myself."
    ),
    AffirmationItem(
        text = "I am dependable."
    ),
    AffirmationItem(
        text = "I am positive and loyal."
    ),
    AffirmationItem(
        text = "I am generous with time, patience, and love to my family."
    ),
    AffirmationItem(
        text = "I am the family member I want to be."
    ),
    AffirmationItem(
        text = "I communicate well with my family."
    ),
    AffirmationItem(
        text = "My family cooperates with each other."
    ),
    AffirmationItem(
        text = "We are a well-balanced family."
    ),
    AffirmationItem(
        text = "My family is healthy and vibrant."
    ),
    AffirmationItem(
        text = "I am a source of love and inspiration for my family."
    ),
    AffirmationItem(
        text = "I encourage my family to achieve their dreams."
    ),
    AffirmationItem(
        text = "I radiate love and happiness to my family."
    ),
    AffirmationItem(
        text = "I am grateful for all that I am, for all that I have, and for all that I experience."
    ),
    AffirmationItem(
        text = "I have an attitude of gratitude."
    ),
    AffirmationItem(
        text = "My thoughts are focused on positivity and thankfulness."
    ),
    AffirmationItem(
        text = "I am sincerely grateful, and this attracts positivity into my life."
    ),
    AffirmationItem(
        text = "I take time to be grateful for all the small things, such as the blue sky or the sound of laughter."
    ),
    AffirmationItem(
        text = "I am grateful for my family, my friends, and my relationships."
    ),
    AffirmationItem(
        text = "I am thankful for simply being alive."
    ),
    AffirmationItem(
        text = "I am thankful for what I have."
    ),
    AffirmationItem(
        text = "I am grateful for all the positive things that are coming my way."
    ),
    AffirmationItem(
        text = "I am grateful for the people in my life."
    ),
    AffirmationItem(
        text = "I release all ill feelings in me about people, incidents and anything else. I forgive everyone associated with me."
    ),
    AffirmationItem(
        text = "The door to my heart opens inward. I move through forgiveness to love."
    ),
    AffirmationItem(
        text = "The past is over, so it has no power now. The thoughts of THIS moment create my future."
    ),
    AffirmationItem(
        text = "I give myself the gift of freedom from the past and move with joy into the now."
    ),
    AffirmationItem(
        text = "There is no problem so big or small that it cannot be solved with love."
    ),
    AffirmationItem(
        text = "I am ready to be healed. I am willing to forgive. All is well."
    ),
    AffirmationItem(
        text = "I move beyond forgiveness to understanding, and I have compassion for all."
    ),
    AffirmationItem(
        text = "Each day is a new opportunity. Yesterday is over and done."
    ),
    AffirmationItem(
        text = "I know that old, negative patterns no longer limit me. I let them go with ease."
    ),
    AffirmationItem(
        text = "I am forgiving, loving, gentle, and kind, and I know that life loves me."
    ),
    AffirmationItem(
        text = "I accept and experience all my feelings."
    ),
    AffirmationItem(
        text = "It is healthier to express myself clearly and directly."
    ),
    AffirmationItem(
        text = "It is safe for me to be vulnerable."
    ),
    AffirmationItem(
        text = "I can reward myself for trying new things."
    ),
    AffirmationItem(
        text = "My dreams can come true."
    ),
    AffirmationItem(
        text = "I choose life. I choose happiness."
    ),
    AffirmationItem(
        text = "I trust my body, I trust myself."
    ),
    AffirmationItem(
        text = "I am now clearing my negative beliefs."
    ),
    AffirmationItem(
        text = "I have the freedom and power to create the life I desire."
    ),
    AffirmationItem(
        text = "I forgive myself completely."
    ),
    AffirmationItem(
        text = "I am too big a gift to this world to feel self-pity and sadness."
    ),
    AffirmationItem(
        text = "I love and approve of myself."
    ),
    AffirmationItem(
        text = "I am competent, smart, and able."
    ),
    AffirmationItem(
        text = "I believe in myself."
    ),
    AffirmationItem(
        text = "I recognize the many good qualities I have."
    ),
    AffirmationItem(
        text = "I surround myself with people who bring out the best in me, and I see the best in others."
    ),
    AffirmationItem(
        text = "I let go of negative thoughts and feelings about myself."
    ),
    AffirmationItem(
        text = "I love who I am. I love who I have become."
    ),
    AffirmationItem(
        text = "I am always growing and developing."
    ),
    AffirmationItem(
        text = "I love and accept myself just as I am."
    ),
    AffirmationItem(
        text = "Every day, in every way, I am getting healthier and healthier and feeling better and better."
    ),
    AffirmationItem(
        text = "I love myself and I am perfectly healthy."
    ),
    AffirmationItem(
        text = "Every cell in my body is health conscious."
    ),
    AffirmationItem(
        text = "I am full of energy and vitality and my mind is calm and peaceful."
    ),
    AffirmationItem(
        text = "I think only positive thoughts and am always happy and joyous, no matter what the external conditions are."
    ),
    AffirmationItem(
        text = "I always feel good. As a result, my body feels good and I radiate good feelings."
    ),
    AffirmationItem(
        text = "Every day is a new day full of hope, happiness, and health."
    ),
    AffirmationItem(
        text = "I treat my body as a temple. It is holy, it is clean, and it is full of goodness."
    ),
    AffirmationItem(
        text = "I breathe deeply, exercise regularly and feed only good nutritious food to my body."
    ),
    AffirmationItem(
        text = "I am free of diabetes, free of blood pressure problems, free of cancerous cells, and free of all life-threatening diseases."
    ),
    AffirmationItem(
        text = "I am abundant."
    ),
    AffirmationItem(
        text = "Abundance flows through my open arms."
    ),
    AffirmationItem(
        text = "I am connected to the abundance of the universe."
    ),
    AffirmationItem(
        text = "I am financially free."
    ),
    AffirmationItem(
        text = "I am grateful for the riches in my life."
    ),
    AffirmationItem(
        text = "I am wealthier each day."
    ),
    AffirmationItem(
        text = "I believe in my unlimited prosperity."
    ),
    AffirmationItem(
        text = "I create money easily and effortlessly."
    ),
    AffirmationItem(
        text = "I have more than enough money to pay my bills."
    ),
    AffirmationItem(
        text = "I have wealth in every area of my life."
    ),
    AffirmationItem(
        text = "I am respected by everyone in my work environment."
    ),
    AffirmationItem(
        text = "I am acknowledged for all my good efforts at work."
    ),
    AffirmationItem(
        text = "I love and enjoy my work and I receive the perfect pay."
    ),
    AffirmationItem(
        text = "I am a magnet for success."
    ),
    AffirmationItem(
        text = "Success flows easily into my open arms."
    ),
    AffirmationItem(
        text = "My employer recognizes my hard work and give s me a huge raise."
    ),
    AffirmationItem(
        text = "Every day I wake up and go to the best job in the world."
    ),
    AffirmationItem(
        text = "I know my calling and the work I am supposed to do in my life."
    ),
    AffirmationItem(
        text = "The type of work I do is miraculously in complete demand."
    ),
    AffirmationItem(
        text = "I have created the perfect business for myself and my pay is overwhelming."
    ),
)