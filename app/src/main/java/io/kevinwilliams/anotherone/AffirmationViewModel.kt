package io.kevinwilliams.anotherone

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel

class AffirmationViewModel: ViewModel() {

    private var currentPosition by mutableStateOf(79)
    var affirmationItems = mutableStateListOf<AffirmationItem>()
        private set

    var currentAffirmation: AffirmationItem? = null
        get() = affirmationItems.getOrNull(currentPosition)

    init {
        updateFilters()
    }

    fun onNext() {
        currentPosition++
        if (currentPosition > affirmationItems.count() - 1) {
            currentPosition = 0
        }
    }

    fun onSelectTag(tag: String?) {
        // TODO: Create a service to filter multiple lists based on tags
        updateFilters()
    }

    private fun updateFilters() {
        affirmationItems.clear()
        affirmationItems.addAll(affirmationList)
    }
}