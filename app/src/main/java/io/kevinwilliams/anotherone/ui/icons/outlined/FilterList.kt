package io.kevinwilliams.anotherone.ui.icons.outlined

import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import io.kevinwilliams.anotherone.R

public val Icons.Outlined.FilterList: ImageVector
    @Composable
    get() {
        if (_filterList != null) {
            return _filterList!!
        }
        _filterList = ImageVector.vectorResource(id = R.drawable.ic_outline_filter_list_24)
        return _filterList!!
    }

private var _filterList: ImageVector? = null