package io.kevinwilliams.anotherone.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = Peach200,
    primaryVariant = Peach500,
    secondary = Flax200
)

private val LightColorPalette = lightColors(
    primary = Peach500,
    primaryVariant = Peach700,
    secondary = Flax200,
    background = Peach200,
    surface = Colors.Blue._100,
    onPrimary = Color.White,
    onBackground = Color.Black,
    onSurface = Color.DarkGray
)

@Composable
fun AnotherOneTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable() () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
            colors = colors,
            typography = Typography,
            shapes = Shapes,
            content = content
    )
}