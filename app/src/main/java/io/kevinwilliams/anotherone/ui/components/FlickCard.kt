package io.kevinwilliams.anotherone.ui.components

import android.util.Log
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.offset
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.IntOffset
import kotlin.math.absoluteValue
import kotlin.math.roundToInt

@Composable
fun FlickCard(
    orientation: Orientation,
    content: @Composable() () -> Unit
) {
    val offsetPosition = remember { mutableStateOf(0f) }
    BoxWithConstraints() {
        val flickCardScope = this
        val maxDimension = with(LocalDensity.current) {
            when (orientation) {
                Orientation.Vertical -> flickCardScope.maxHeight.toPx()
                Orientation.Horizontal -> flickCardScope.maxWidth.toPx()
            }
        }

        Box(modifier = Modifier
            .draggable(
                rememberDraggableState { delta ->
                    val newValue = offsetPosition.value + delta
                    offsetPosition.value = newValue.coerceIn(-maxDimension, maxDimension)
                },
                orientation = orientation,
                onDragStopped = { velocity ->
                    Log.i("onDragStopped", velocity.toString())
                    val midPoint = offsetPosition.value + (maxDimension / 2)
                    val midPointSlide = midPoint + (velocity/10)
                    if (midPointSlide < 0 || midPointSlide > maxDimension) {
                        Log.i("onDragStopped:toDismiss", midPointSlide.toString())
                    }
                }
            )
            .offset {
                when (orientation) {
                    Orientation.Vertical -> IntOffset(0, offsetPosition.value.roundToInt())
                    Orientation.Horizontal -> IntOffset(offsetPosition.value.roundToInt(), 0)
                }
            }
        ) {
            content()
        }
    }
}