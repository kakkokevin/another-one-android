package io.kevinwilliams.anotherone.ui.theme

import androidx.compose.ui.graphics.Color

val Peach50 = Color(0xffe3f2fd)
val Peach100 = Color(0xfffccebe)
val Peach200 = Color(0xfffaae95)
val Peach500 = Color(0xfffa5e26)
val Peach700 = Color(0xffe2511d)
val Peach900 = Color(0xffbb3d11)
val Flax200 = Color(0xfffae195)
val Flax500 = Color(0xfff9c33b)
val Flax700 = Color(0xfff6a431)
val Flax900 = Color(0xfff47824)

val Cream = Color(0xfffefbea)

object Colors {
    object Blue {
        val _50 = Color(0xffe3f2fd)
        val _100 = Color(0xffbbdefb)
        val _200 = Color(0xff90caf9)
        val _300 = Color(0xff64b5f6)
        val _400 = Color(0xff42a5f5)
        val _500 = Color(0xff2196f3)
        val _600 = Color(0xff1e88e5)
        val _700 = Color(0xff1976d2)
        val _800 = Color(0xff1565c0)
        val _900 = Color(0xff0d47a1)
    }

    object Red {
        val _50 = Color(0xffffebee)
        val _100 = Color(0xffffcdd2)
        val _200 = Color(0xffef9a9a)
        val _300 = Color(0xffe57373)
        val _400 = Color(0xffef5350)
        val _500 = Color(0xfff44336)
        val _600 = Color(0xffe53935)
        val _700 = Color(0xffd32f2f)
        val _800 = Color(0xffc62828)
        val _900 = Color(0xffb71c1c)
    }

//    object Red {
//        val _50 = Color(0xff)
//        val _100 = Color(0xff)
//        val _200 = Color(0xff)
//        val _300 = Color(0xff)
//        val _400 = Color(0xff)
//        val _500 = Color(0xff)
//        val _600 = Color(0xff)
//        val _700 = Color(0xff)
//        val _800 = Color(0xff)
//        val _900 = Color(0xff)
//    }
}