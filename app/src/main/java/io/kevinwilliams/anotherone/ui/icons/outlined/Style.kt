package io.kevinwilliams.anotherone.ui.icons.outlined

import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import io.kevinwilliams.anotherone.R

public val Icons.Outlined.Style: ImageVector
    @Composable
    get() {
        if (_style != null) {
            return _style!!
        }
        _style = ImageVector.vectorResource(id = R.drawable.ic_outline_style_24)
        return _style!!
    }

private var _style: ImageVector? = null