package io.kevinwilliams.anotherone.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp

@Composable
fun RoundButton(
    icon: ImageVector,
    contentDescription: String,
    onClick: () -> Unit
) {
    Surface(
        modifier = Modifier
            .width(32.dp)
            .height(32.dp)
            .clip(CircleShape),
        elevation = 1.dp
    ) {
        Icon(
            icon,
            contentDescription,
            modifier = Modifier
                .clip(CircleShape)
                .clickable { onClick(); }
                .padding(4.dp)
        )
    }
}