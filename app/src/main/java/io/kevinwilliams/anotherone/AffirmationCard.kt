package io.kevinwilliams.anotherone

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.style.TextAlign

@Composable
fun AffirmationCard(affirmation: AffirmationItem?) {
    val affirmation = affirmation ?: AffirmationItem(text = "I got this.")

    Box(modifier = Modifier
        .fillMaxHeight()
        .fillMaxWidth()
    ) {
        Column(modifier = Modifier.align(Alignment.Center)) {
            val style = MaterialTheme.typography.h3.copy(
                color = Color.DarkGray,
                shadow = Shadow(Color.White, Offset(4f, 4f), 2f)
            )
            Text(
                affirmation.text,
                style = style,
                textAlign = TextAlign.Center
            )
        }
    }
}