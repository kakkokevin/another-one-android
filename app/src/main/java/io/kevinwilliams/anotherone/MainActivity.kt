package io.kevinwilliams.anotherone

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import io.kevinwilliams.anotherone.ui.theme.AnotherOneTheme

class MainActivity : ComponentActivity() {

    val affirmationViewModel by viewModels<AffirmationViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AnotherOneTheme {
                Surface(color = MaterialTheme.colors.background) {
                    AffirmationActivityScreen(affirmationViewModel)
                }
            }
        }
    }
}

@Composable
private fun AffirmationActivityScreen(vm: AffirmationViewModel) {
    AffirmationScreen(affirmations = vm.affirmationItems)
}